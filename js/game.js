// Initialize Phaser
var game = new Phaser.Game(400, 600, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 ,stage: 1};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('over', overState);
// Start the 'boot' state
game.state.start('boot');