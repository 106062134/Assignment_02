var overState = {
    preload: function () {
        console.log('over');
    },
    create: function() {
        // Add a background image
        game.stage.backgroundColor = '#202020';
        console.log(game.global.stage)
        // Display the name of the game
        // Explain how to start the game
        if(game.global.stage != 1){
            var nameLabel = game.add.text(game.width/2, game.height/2-70 , 'you win',{ font: '50px Arial', fill: '#ffffff' });
            var startLabel = game.add.text(game.width/2, game.height/2+50,'press space to continue', { font: '25px Arial', fill: '#ffffff' });
        }
        else{
            var nameLabel = game.add.text(game.width/2, game.height/2-70 , 'game over',{ font: '50px Arial', fill: '#ffffff' });
            var startLabel = game.add.text(game.width/2, game.height/2+50,'press space to menu', { font: '25px Arial', fill: '#ffffff' });
        }
        nameLabel.anchor.setTo(0.5, 0.5);
        startLabel.anchor.setTo(0.5, 0.5);
        
        // Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width/2, game.height/2,'your score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
        
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upKey.onDown.add(this.start, this);
    },
    start: function() {
        // Start the actual game
        if(game.global.stage != 1){
            game.state.start('play');
        }else{
            game.state.start('menu');
        }
    },
};