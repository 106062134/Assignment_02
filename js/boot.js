var bootState = {
    preload: function () {
        console.log('boot');
    },
    create: function() {
        // Set some game settings.
        game.stage.backgroundColor = '#FFFFFF';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        // Start the load state.
        game.state.start('load');
    }
};