var playState = { preload: preload, create: create, update: update, render: render }

function preload() {
    console.log('play');
}

var player;
var aliens;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
var starfield;
//var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var enemy_num;
var skills;
var shoo;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);
    enemy_num = game.global.stage*5;

    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

    //music
    shoo = game.add.audio('shoo');
    shoo.volume = 0.1;

    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    
    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    /*player = game.add.sprite(game.width/2, game.height-50, 'ship');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.collideWorldBounds = true;*/

    player = game.add.sprite(game.width/2, game.height-50, 'player');
    player.facingLeft = false;
    player.anchor.setTo(0.5, 0.5);
    game.physics.arcade.enable(player);
    player.animations.add('rightwalk', [1, 2], 8, true);
    player.animations.add('leftwalk', [3, 4], 8, true);
    player.animations.add('rightjump', [5, 6], 16, true);
    player.animations.add('leftjump', [7, 8], 16, true);

    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;
    
    //createAliens();
    aliens.createMultiple(10, 'invader');
    game.time.events.loop(500, addEnemy, this);

    //  The score
    if(game.global.stage == 1) game.global.score = 0;
    scoreString = 'Score : ';
    scoreText = game.add.text(10, 10, scoreString + game.global.score, { font: '34px Arial', fill: '#fff' });

    //  Lives
    lives = game.add.group();
    game.add.text(game.world.width - 100, 10, 'Lives : ', { font: '24px Arial', fill: '#fff' });

    //skills
    skills = game.add.group();
    game.add.text(game.world.width - 100, 85, 'skill : ', { font: '24px Arial', fill: '#fff' });

    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    for (var i = 0; i < 3; i++) 
    {
        var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'player');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.4;
    }
    for (var i = 0; i < 3; i++) 
    {
        var ship = skills.create(game.world.width - 100 + (30 * i), 130, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.4;
    }

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    
}

function addEnemy() {
    var alien = aliens.getFirstDead();
    if (!alien) { return;}
    alien.anchor.setTo(0.5, 1);

    var dir = Math.random();
    if(dir <= 0.5){
        alien.reset(game.rnd.pick([game.width/2-50, game.width/2+50]), 0);
        alien.body.gravity.y = game.global.stage * 50;
    }
    else if(dir < 0.7 && dir > 0.5){
        alien.reset(0, game.rnd.pick([50, game.height-50]));
        alien.body.velocity.x = game.global.stage * 50;
    }
    else{
        alien.reset(game.width, game.rnd.pick([50, game.height-50]));
        alien.body.velocity.x = 0 - game.global.stage * 50;
    }
    
    alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
    alien.play('fly');
    alien.checkWorldBounds = true;
    alien.outOfBoundsKill = true;
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function update() {

    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);
        player.angle += 1;

        /*if (cursors.left.isDown)
        {
            player.body.velocity.x = -200;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 200;
        }
        else if (cursors.up.isDown)
        {
            player.body.velocity.y = -200;
        }
        else if (cursors.down.isDown)
        {
            player.body.velocity.y = 200;
        }*/
        if (cursors.left.isDown) {
            player.body.velocity.x = -200;
            player.facingLeft = true;
            player.animations.play('leftwalk');
        }
        else if (cursors.right.isDown) { 
            player.body.velocity.x = 200;
            player.facingLeft = false;
            player.animations.play('rightwalk'); 
        }   
        else if (cursors.up.isDown) {
                player.body.velocity.y = -200;
        }
        else if (cursors.down.isDown) { 
            player.body.velocity.y = 200;
        }
        else {
            player.body.velocity.x = 0;
            if(player.facingLeft) {
                player.frame = 3;
            }else {
                player.frame = 1;
            }
            player.animations.stop();
        } 

        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
            if(player.facingLeft) {
                player.animations.play('leftjump');
            }else {
                player.animations.play('rightjump');
            }
        }

        if (game.time.now > firingTimer)
        {
            enemyFires();
        }

        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(player, aliens, enemycrushPlayer, null, this);
    }

}

function render() {

    // for (var i = 0; i < aliens.length; i++)
    // {
    //     game.debug.body(aliens.children[i]);
    // }

}

function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();

    //  Increase the score
    game.global.score += 20;
    scoreText.text = scoreString + game.global.score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    //if (aliens.countLiving() == 0)
    enemy_num -= 1;
    if(!enemy_num)
    {
        game.global.stage += 1;

        enemyBullets.callAll('kill',this);
        game.state.start('over');
        /*stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);*/
    }
}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');
        game.global.stage = 1;

        game.state.start('over');
        /*stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);*/
    }

}

function enemycrushPlayer (player, alien) {
    
    alien.kill();

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');
        game.global.stage = 1;

        game.state.start('over');
        /*stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);*/
    }

}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(enemyBullet,player,120);
        firingTimer = game.time.now + 500;
    }

}

function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
            shoo.play();
        }
    }

}

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function restart () {

    //  A new level starts
    
    //resets the life count
    lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;

}