var loadState = {
    preload: function () {
        console.log('load');
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        /*var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);*/
        // Load all game assets
        // Load a new asset that we will use in the menu state
        game.load.spritesheet('player', 'assets/MARIO.png', 32, 32);
        game.load.image('ship', 'assets/player.png');
        game.load.image('bullet', 'assets/bullet.png');
        
        game.load.spritesheet('invader', 'assets/invader32x32x4.png', 32, 32);
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.image('starfield', 'assets/starfield.png');
        game.load.image('background', 'assets/background2.png'); //no use
        game.load.image('pixel', 'assets/pixel.png');

        game.load.audio('cancan', 'assets/Cancan.ogg');
        game.load.audio('shoo', 'assets/bullet.wav');
    },
    create: function() {
        // Go to the menu state
        //music
        music = game.add.audio('cancan');
        music.loopFull(0.2);
        game.state.start('menu');
    }
}; 