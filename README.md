# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|

|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|N|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
 基本架構
 boot -> load -> menu -> play -> over ，結束會回到menu

play.js:各項說明
    player: 馬力歐的頭，因為要做動畫，然後找不到圖就用它了
        主要有的功能是move, fire, collision
        原本想要嘗試讓它旋轉然後可以從頭發射，但沒有成功所以現在只會旋轉
    enemy: phaser素材包的東西，會隨機從地圖某個方向生出，然後定時向角色發射子彈攻擊
    bullet: 就是子彈，用group包 起來操作比較方便

    互動: collision ，C(3,2)=3 所以寫三個function
    控制角色物件是否移除，生命計算，還有關卡結束的判定，分數計算都會在裡面完成
# Basic Components Description : 
1. Jucify mechanisms :
    1. level: 把敵人的移動速度加快，整個螢幕會越來越亂，也死得越快
    2. skill: 只做了計次的格子，但是沒想到要做甚麼技能
2. Animations : EXPLOOOOSION
    好的，被子彈打到，或者是碰撞發生，不管敵人還是自己都會爆炸爆炸
3. Particle Systems : [NO?]
4. Sound effects : 
    1. 遊戲基本的音樂，手邊不知道為甚麼有一首ogg檔的就拿來用了，用Loop的方式撥放
    2. 發射子彈的聲音，單次撥放，跟背景音樂完全不搭，非常爛的組合
    3. 不能調整大小跟靜音，我有直接幫調小聲了
5. Leaderboard : [NO]




# Bonus Functions Description : 
game.physics.arcade.moveToObject(enemyBullet,player,120);
用裡面有的API就可以實現追蹤??的子彈，這樣不知道有沒有 總之發射的時候假設別人不動是可以打中任何方向的
